/**
 * For starting - create car with consumption:
 * var car = Car(5);
 *
 * Car module. Created car with custom consumption.
 * @param consumption {number}
 * @param consOfoil {number}
 * @return {{
 *            start: start,
*             riding: riding,
 *            checkGas: checkGas,
  *           refueling: refueling
*           }}
 * @constructor
 */
function Car(consumption,consOfoil,maxSpeed) {
  if (!isNumeric(consumption)) {
    showMessage('Wrong consumption of gas', 'error');
    return;
  }
  if (!isNumeric(consOfoil)) {
    showMessage('Wrong consumption of oil', 'error');
    return;
  }
  if (!isNumeric(maxSpeed)) {
    showMessage('Wrong maxSpeed', 'error');
    return;
  }

  let gasBalance = 100;
  let gasConsumption = consumption / 100;
  let gasResidue;
  let gasVolume = 200;
  let ignition = false;
  let ready = false;
  let oilBalance = 5;
  let oilConsumption = consOfoil/100;
  let oilResidue;
  let oilVolume = 5;
  const MAXSPEED = maxSpeed;

  /**
	 * Check gas amount after riding.
	 * @param distance {number} - Riding distance.
	 * @return {number} - Gas amount.
	 */
  function gasCheck(distance) {// остаток бензина после поездки
    if (gasBalance <= 0) {
      return 0;
    }

    let gasForRide = Math.round(distance * gasConsumption);

    return (gasBalance - gasForRide);
  }
  function oilCheck(distance) {// остаток масла- после поездки
    if (oilBalance <= 0) {
      return 0;
    }

    let oilForRide = Math.round(distance * oilConsumption);

    return (oilBalance - oilForRide);
  }

  /**
	 * Show message for a user in the console.
	 * @param message {string} - Text message for user.
	 * @param type {string} - Type of the message (error, warning, log). log by default.
	 */
  function showMessage(message, type) {
    let messageText = message ? message : 'Error in program. Please call - 066 083 07 06';

    switch (type) {
      case 'error':
        console.error(messageText);
        break;
      case 'warning':
        console.warn(messageText);
        break;
      case 'log':
        console.log(messageText);
        break;
      default:
        console.log(messageText);
        break;
    }
  }

  /**
	 * Check car for ride.
	 * @param distance {number} - Ride distance.
	 */
  function checkRide(distance) {
    gasResidue = gasCheck(distance);
    oilResidue = oilCheck(distance);
  }
  

  /**
	 * Check value to number.
	 * @param n {void} - value.
	 * @return {boolean} - Is number.
	 */
  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  /**
	 * Public methods object.
	 */
  return {
    /**
		 * Start car.
		 */
    start: function() {
      ignition = true;

      if (gasBalance <= 0) {
        showMessage('You don\'t have gas. Please refuel the car.', 'error');
        ready = false;
        return;
      }
      if (oilBalance <= 0) {
        showMessage('You don\'t have oil. Please reoil the car.', 'error');
        ready = false;
        return;
      }

      ready = true;

      showMessage('Ingition', 'log');
    },

    /**
		 * Riding function.
		 * @param distance {number} - Riding distance.
		 */
    riding: function(distance,speed) {
      if (!isNumeric(distance)) {
        showMessage('Wrong distance', 'error');
        return;
      }
      if (!isNumeric(speed)) {
        showMessage('Wrong speed', 'error');
        return;
      }

      if (!ignition) {
        showMessage('You need start car', 'error');
        return;
      }

      if (!ready) {
        ignition = false;
        showMessage('You need gas station', 'error');
        return;
      }
      if(speed > MAXSPEED) {
        showMessage('Your speed is too fast', 'error');
        return;
      }

      checkRide(distance);

      
      let gasDriven = Math.round(gasBalance / gasConsumption); //пройденное расстояние на бензине 
      let oilDriven = Math.round(oilBalance / oilConsumption); //пройденное расстояние на масле
      
      if (gasResidue <= 0 && oilResidue <=0) {
        if(gasDriven <= oilDriven)  {
          gasIsOver(distance);
        }
        if(gasDriven >= oilDriven) {
          oilIsOver(distance);
        }
      }else if (gasResidue <= 0) {
        gasIsOver(distance);
      }else if (oilResidue <= 0) {
        oilIsOver(distance);
      }else if (gasResidue > 0 && oilResidue > 0) {
        gasBalance = gasResidue;
        oilBalance = oilResidue;
        showMessage('You arrived. Gas balance - ' + gasBalance + ' Oil Balance ' + oilBalance, 'log');
        let drivenTime = timeDriven(distance, speed);
        showMessage('Your time for riding is '+ drivenTime);
      }
      function oilIsOver(distance) {
        let distanceLeft = Math.round(distance - oilDriven);
        oilBalance = 0;
        gasBalance = gasBalance - Math.round(gasConsumption * oilDriven); 
        let neededOil = Math.round(oilConsumption * distanceLeft); 
        showMessage('Oil is over. You have driven - '+ oilDriven + ' You need ' + neededOil + " litres. " + distanceLeft + 'km left', 'warning' );
        timeDriven(oilDriven, speed);
        showMessage('Your time for riding is '+ drivenTime);

      }
      function gasIsOver(distance) {
        let distanceLeft = Math.round(distance - gasDriven);
        gasBalance = 0;
        oilBalance =  oilBalance - Math.round(oilConsumption * gasDriven);
        let neededGas = Math.round(gasConsumption * distanceLeft); 
        showMessage('Gas is over. You have driven - '+ gasDriven + ' You need ' + neededGas + " litres. " + distanceLeft + 'km left', 'warning' );
        timeDriven(gasDriven, speed);
        showMessage('Your time for riding is '+ drivenTime);
      }
      function timeDriven(distance, speed) {
        let drivenTime =  distance / speed;
        return drivenTime.toFixed(2);
      }
    },
  
 
    /**
		 * Check gas function.
		 */
    checkGas: function() {
      showMessage('Gas - ' + gasBalance, 'log');
    },

    /**
		 * Refueling function.
		 * @param gas
		 */
    /**
     * Check oil function.
     */
     checkOil: function() {
      showMessage('Oil - ' + oilBalance, 'log');
    },
    refueling: function(gas) {
      if (!isNumeric(gas)) {
        showMessage('Wrong gas amount', 'error');
        return;
      }

      if (gasVolume === gasBalance) {
        showMessage('Gasoline tank is full', 'warning');
      } else if (gasVolume < gasBalance + gas) {
        let excess = Math.round(gasBalance + gas - gasVolume);
        showMessage('Gasoline tank is full. Excess - ' + excess, 'log');
        gasBalance = gasVolume;
      } else {
        gasBalance = Math.round(gasBalance + gas);
        showMessage('Gas balance - ' + gasBalance, 'log');
      }
    },
    reoiling: function(oil) {
      if (!isNumeric(oil)) {
        showMessage('Wrong oil amount', 'error');
        return;
      }

      if (oilVolume === oilBalance) {
        showMessage('Oil is full', 'warning');
      } else if (oilVolume < oilBalance + oil) {
        let excess = Math.round(oilBalance + oil - oilVolume);
        showMessage('Oil is full. Excess - ' + excess, 'log');
        oilBalance = oilVolume;
      } else {
        oilBalance = Math.round(oilBalance + oil);
        showMessage('Oil balance - ' + oilBalance, 'log');
      }
    },
  };
}
